<?php

Route::group(['prefix' => 'lawyer', 'namespace' => 'YCMS\Lawyer\Http\Controllers'], function()
{
	Route::get('/', 'LawyerController@index');
});