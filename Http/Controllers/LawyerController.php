<?php namespace YCMS\Lawyer\Http\Controllers;

use YCMS\Modules\Routing\Controller;

class LawyerController extends Controller {
	
	public function index()
	{
		return view('lawyer::index');
	}
	
}